webpackJsonp([5],{

/***/ 181:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(4)(
  /* script */
  __webpack_require__(191),
  /* template */
  __webpack_require__(205),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/home/rkaz246/development/projects/beerio-web2/resources/assets/js/components/Logout.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Logout.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e0ec3332", Component.options)
  } else {
    hotAPI.reload("data-v-e0ec3332", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {

    console.log('Login Component mounted.');
    this.logout();
  },

  methods: {
    logout: function logout() {
      this.$auth.destroyToken();
      this.$router.push('/auth/login');
    }
  }
});

/***/ }),

/***/ 205:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div')
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-e0ec3332", module.exports)
  }
}

/***/ })

});