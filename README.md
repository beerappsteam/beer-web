# Beer Web Version #

This is the web app version of the beer application.
For things that need to be done, check our [issue tracker.](https://bitbucket.org/beerappsteam/beer-web/issues)

### What is this repository for? ###

* Anyone that will be working on the Beer project

### External Project Dependencies ###
- [GIT](https://git-scm.com/download/)
- [Composer](https://getcomposer.org/doc/00-intro.md)
- [Docker and Docker-Compose](https://docs.docker.com/engine/installation/)
- [NPM](https://docs.docker.com/engine/installation/)

### How do I get set up? ###

- Clone the repo

```
#!php
$ git clone https://useryourusername@bitbucket.org/beerappsteam/beer-web
```
- Update the submodule

```
#!php
$ git submodule update --init --recursive
```
- Rename .env.example to .env in the root project location
- Run docker: 

```
#!php
$ docker-compose up  mysql apache2
```

- For javascript(Vue): 

```
#!javascript
$ npm run watch
```

- Run and seed migrations in the laradock folder via bash inside the workspace


```
#!php
$ cd laradock
$ docker-compose exec workspace bash
$ composer update //only have to run on first time
$ php artisan migrate --seed //only have to run on first time
$ php artisan passport:install //only have to run on first time
```


- Visit http://localhost/

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* email: ryan.kazokas@gmail.com